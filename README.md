# Internship Interview Questions
*Please include explanation along with your answers.*

1. Please describe yourself using JSON (include your internship start/end date, your current location).

ANSWER:

    internshipCandidate1 = {
        "full_name" : "Hiba Azhari",
        "start_date" : "14-12-2020",
        "end_date" : "14-03-2021",
        "curr_location" : "Subang Jaya, Selangor",
        "course_info" : {
            "major" : "Computer Science",
            "university" : "Sunway Lancaster",
            "year_of_study" : 3
        },
        "hobbies" : ["singing","reading"]
    };
 
2. Tell us about a newer (less than five years old) web technology you like and why?

ANSWER:

    I'm interested in the way AI chatbots are being used in a growing number of websites recently. It's exciting to see the developments happening
    to make them more human-like. Learing AI algorithms and concepts in my classes and otherwise makes chatbots interesting to me as I can slowly  
    begin to appreciate what goes on behind the scenes and how they can eventually process natural language.


3. In Java, the maximum size of an Array needs to be set upon initialization. Supposedly, we want something like an Array that is dynamic, such that we can add more items to it over time. Suggest how we can accomplish that (other than using ArrayList)?

ANSWER:

    One way to handle this is to essentially recreate the ArrayList as a new custom class:
    - The class would take in the data type that the array is to hold
    - Then, it would create an initial array of a given size eg.64.
    - It would have a pointer to track the index of the latest added item
    - It would have an "Add" function that inserts new items and increments the index
    - The function checks if the array is filling up, in which case it would create a new array 
        of double the size and copy all items over.

4. Explain this block of code in Big-O notation.
    ```
    void sampleCode(int arr[], int size)
    {
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                printf("%d = %d\n", arr[i], arr[j]);
            }
         }
    }
    ```

ANSWER:

    - This code contains a pair of nested loops in a function. 
    - The inner loop executes a simple O(1) print operation n times (n = input array size)
    - The outer loop calls the inner loop n times
    - Therefore, the function's time complexity is O(n^2)

5. One of our developers built a page to display market information for over 500 cryptocurrencies in a single page. After several days, our support team started receiving complaints from our users that they are not able to view the website and it takes too long to load. What do you think causes this issue and suggest at least 3 ideas to improve the situation.

ANSWER:

    - The slow page loading is likely due to a large number of Api calls used to render the page. A few solutions could be:
        - Revising the code: The way the data is being retrieved can be revised to see if there it can be fetched with fewer api calls.
        - Using a cache: the Api responses could be cached locally and displayed while updated information is fetched at regular intervals,
          reducing the frequency of calls needed.
        - Asynchronous calls: the calls to Api can be made in parallel instead of in sequence using an async function

6. In Javascript, What is a "closure"? How does JS Closure works?

ANSWER:

    - A closure in Javascript is the combination of a function and the variables accessible to it. 
    - Functions in javascript, by default, have access to the variables within them, the variables in any outer/enclosing function,
      and to global variables.
    - If a function is called in a different setting from that in which it was declared, a closure allows it to 'remember
      the variables that where accessible to it at time of declaration.
    - For example, Function A is declared where it has access to variable B (a variable belonging to an enclosing function).
      If Function A is called in another setting where B is inaccessible, Function A can still access B thanks to closures.

7. In Javascript, what is the difference between var, let, and const. When should I use them?

ANSWER:

    'var', 'let' and 'const' are used to declare variables in Javascript. The differences between them are in:
        1. Scope
        2. Modification

    - Variables declared with 'var' are globally scoped when declared outside a function, and locally scoped when declared within a function.
    - Once they are accessed, they can be reassigned or updated
    - They can be used for variables that will be accessed and modified from multiple points in the code
    - 
    - Variables declared with 'let' are locally scoped within the block where they were declared
    - Once they are accessed, they can be updated but not reassigned
    - 'let' can be used to assign a value to a variable locally without affecting its global value, For example, if we have a global 
      variable "var price = 2", but in a specific code block we need it to be "price = 3". We can use 'let price = 3' 
      and it will only take effect within that block
    -
    - Variables declared with 'const' are locally scoped within the block where they were declared
    - They cannot be reassigned nor updated once declared (however their contents may be updated)
    - They can be used for values that are to remain constant throughout the code

8. Share with us one book that has changed your perspective in life. How did it change your life?

ANSWER:

    One of the books that had a major impact on me was "The Compound Effect" by Darren Hardy. The book discusses the "compound effect", 
    which is the seemingly magical result of small actions done over a long period of time. It got me to appreciate the cliche "time is
    your most valuable asset", and to observe my daily habbits and where they're taking me in the long term.

9. What is the thing you believe in that is true, that most people disagree with?

ANSWER:

    Wired headphones/earphones are superior to bluetooth and people are in denial. They cost less, don't get lost, don't run out of battery,
    are easier to connect and easier to switch from one device to another. The feeling of freedom with bluetooth is tempting but overall
    quality of life is just better with wired earphones.

10. What are your thoughts on the subject of Arts and Humanities?

ANSWER:

    I believe that understanding of these subjects is an important part of any person's life regardless of their field/career.
    They help us connect better with our identities and express ideas that are difficult to logically explain. I believe they are 
    best taught without the pressure of structured education (exams/evaluation) as that often takes away from their value.

---
# Simple Coding Assessment

Build a Cryptocurrency Market dashboard showing market data.

requirement:
1. using the API endpoints found in https://www.coingecko.com/en/api, build a cryptocurrency market dashboard page.
2. The page should be able to list at least 20 coins.
3. The page should show price, volume, name, symbol of the coin.
4. The page should show the graph of 7 days data, using the **sparkline** returned from api. For example sparkline data can be obtained using [https://api.coingecko.com/api/v3/coins/bitcoin?sparkline=true](https://api.coingecko.com/api/v3/coins/bitcoin?sparkline=true) or [https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&sparkline=true](https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&sparkline=true)
5. The page should allow user to "favorite" coins.
6. build another page "favorite", to only show the favorite coins.
5. **(bonus)** The page should allow user to switch currency for ("usd", "myr", "btc"). The price and volume should display the number in the currency selected.
6. **(bonus)** Host this on a website, or a mobile app.
7. We will schedule a video call with you should we decide to proceed with your interview.
8. You must be able to demo your submission to us.

---
# Submission instruction

1. Fork this repo.
2. Creates a Merge Request in this repo after completion.
